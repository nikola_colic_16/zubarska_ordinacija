<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;
    use \PDO;

    class UslugaModel extends Model {
        protected function getFields() {
            return [
                'usluga_id'         => new Field(
                                    (new NumberValidator())
                                        ->setInteger()
                                        ->setUnsigned()
                                        ->setMaxIntegerDigits(10), false),
                'naziv'             => new Field(
                                    (new StringValidator())
                                        ->setMinLength(1)
                                        ->setMaxLength(65)),
                'opis'              => new Field(
                                    (new StringValidator())
                                        ->setMinLength(1)
                                        ->setMaxLength(64000)),
                'kataloski_broj'    => new Field(
                                    (new NumberValidator())
                                        ->setInteger()
                                        ->setUnsigned()
                                        ->setMaxIntegerDigits(10)),
                'tip_usluge_id'     => new Field(
                                    (new NumberValidator())
                                        ->setInteger()
                                        ->setUnsigned()
                                        ->setMaxIntegerDigits(10)),
                'is_visible'        => new Field
                                    (new BitValidator())
            ];
        }
        
        
        public function getCenePoKategoriji($uslugaId){
            $pdo = $this->getDatabaseConnection()->getConnection();
            $sql = 'SELECT kp.kategorija_pacijenta_id, kp.naziv, uk.cena FROM usluga u INNER JOIN usluga_kategorija_pacijenta uk ON u.usluga_id = uk.usluga_id INNER JOIN kategorija_pacijenta kp ON uk.kategorija_pacijenta_id = kp.kategorija_pacijenta_id WHERE u.usluga_id = ' . $uslugaId . ' ORDER BY kp.kategorija_pacijenta_id;';
            $prep = $pdo->prepare($sql);
            $items = [];

            if ($prep) {
                $res = $prep->execute();

                if ($res) {
                    $items = $prep->fetchAll(PDO::FETCH_OBJ);
                }
            }
            
            return $items;
        }

    }
