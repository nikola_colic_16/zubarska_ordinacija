<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use \PDO;

    class PaketKategorijaPacijentaModel extends Model {
        protected function getFields() {
            return [
                'paket_kategorija_pacijenta_id'             => new Field(
                                                        (new NumberValidator())
                                                            ->setInteger()
                                                            ->setUnsigned()
                                                            ->setMaxIntegerDigits(10), false),
                'paket_id'                                  => new Field(
                                                        (new NumberValidator())
                                                            ->setInteger()
                                                            ->setUnsigned()
                                                            ->setMaxIntegerDigits(10)),
                'kategorija_pacijenta_id'                   => new Field(
                                                        (new NumberValidator())
                                                            ->setInteger()
                                                            ->setUnsigned()
                                                            ->setMaxIntegerDigits(10)),
                'cena'                                      => new Field
                                                        (new NumberValidator())
            ];
        }

        public function getByPaketKategorija($paket_id, $kategorija_pacijenta_id){
            $pdo = $this->getDatabaseConnection()->getConnection();
            $sql = 'SELECT * FROM paket_kategorija_pacijenta WHERE paket_id = ? AND kategorija_pacijenta_id = ?;';
            $prep = $pdo->prepare($sql);
            $item = null;

            if ($prep) {
                $prep->execute( [ $paket_id, $kategorija_pacijenta_id ] );
    
                $item = $prep->fetch(PDO::FETCH_OBJ);

                if (!$item) {
                    $item = null;
                }
            }

            return $item;
        }
    }
