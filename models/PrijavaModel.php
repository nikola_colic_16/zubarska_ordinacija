<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\DateTimeValidator;
    use App\Validators\IpAddressValidator;
    use App\Validators\BitValidator;

    class PrijavaModel extends Model {
        protected function getFields() {
            return [
                'prijava_id'        => new Field(
                                        (new NumberValidator())
                                            ->setInteger()
                                            ->setUnsigned()
                                            ->setMaxIntegerDigits(10), false),
                'created_at'        => new Field(new DateTimeValidator(), false),
                'korisnik_id'       =>  new Field(
                                        (new NumberValidator())
                                            ->setInteger()
                                            ->setUnsigned()
                                            ->setMaxIntegerDigits(10)),
                'ip_adresa'    => new Field(new IpAddressValidator()),
                'is_valid'       => new Field(new BitValidator())
                
            ];
        }

    }
