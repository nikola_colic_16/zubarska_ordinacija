<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;

    class PaketModel extends Model {
        protected function getFields() {
            return [
                'paket_id'                      => new Field(
                                                (new NumberValidator())
                                                    ->setInteger()
                                                    ->setUnsigned()
                                                    ->setMaxIntegerDigits(10), false),
                'naziv'                         => new Field(
                                                (new StringValidator())
                                                    ->setMinLength(1)
                                                    ->setMaxLength(45))
            ];
        }
    }
