<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\DateTimeValidator;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;
    use \PDO;

    class UslugaKategorijaPacijentaModel extends Model {
        protected function getFields() {
            return [
                'usluga_kategorija_pacijenta_id'    => new Field(
                                                (new NumberValidator())
                                                    ->setInteger()
                                                    ->setUnsigned()
                                                    ->setMaxIntegerDigits(10), false),
                'usluga_id'                         => new Field(
                                                (new NumberValidator())
                                                    ->setInteger()
                                                    ->setUnsigned()
                                                    ->setMaxIntegerDigits(10)),
                'kategorija_pacijenta_id'       => new Field(
                                                (new NumberValidator())
                                                    ->setInteger()
                                                    ->setUnsigned()
                                                    ->setMaxIntegerDigits(10)),
                'cena'                          => new Field
                                                (new NumberValidator())
            ];
        }

        public function getByUslugaKategorija($usluga_id, $kategorija_pacijenta_id){
            $pdo = $this->getDatabaseConnection()->getConnection();
            $sql = 'SELECT * FROM usluga_kategorija_pacijenta WHERE usluga_id = ? AND kategorija_pacijenta_id = ?;';
            $prep = $pdo->prepare($sql);
            $item = null;

            if ($prep) {
                $prep->execute( [ $usluga_id, $kategorija_pacijenta_id ] );
    
                $item = $prep->fetch(PDO::FETCH_OBJ);

                if (!$item) {
                    $item = null;
                }
            }

            return $item;
        }
    }
