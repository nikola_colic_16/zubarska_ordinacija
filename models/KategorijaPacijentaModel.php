<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\DateTimeValidator;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class KategorijaPacijentaModel extends Model {
        protected function getFields() {
            return [
                'kategorija_pacijenta_id'       => new Field(
                                                (new NumberValidator())
                                                    ->setInteger()
                                                    ->setUnsigned()
                                                    ->setMaxIntegerDigits(10), false),
                'naziv'                         => new Field(
                                                (new StringValidator())
                                                    ->setMinLength(1)
                                                    ->setMaxLength(15))
            ];
        }
    }
