<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\DateTimeValidator;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use App\Validators\BitValidator;

    class PacijentModel extends Model {
        protected function getFields() {
            return [
                'pacijent_id'               => new Field(
                                            (new NumberValidator())
                                                ->setInteger()
                                                ->setUnsigned()
                                                ->setMaxIntegerDigits(10), false),
                'ime'                       => new Field(
                                            (new StringValidator())
                                                ->setMinLength(1)
                                                ->setMaxLength(45)),
                'prezime'                   => new Field(
                                            (new StringValidator())
                                                ->setMinLength(1)
                                                ->setMaxLength(45)),
                'adresa'                    => new Field(
                                            (new StringValidator())
                                                ->setMinLength(1)
                                                ->setMaxLength(64000)),
                'born_at'                   => new Field(
                                            (new DateTimeValidator())
                                                ->disallowTime()),
                'telefon'                   => new Field(
                                            (new StringValidator())
                                                ->setMinLength(1)
                                                ->setMaxLength(45)),
                'created_at'                => new Field
                                            (new DateTimeValidator(), false),
                'kategorija_pacijenta_id'   => new Field(
                                            (new NumberValidator())
                                                ->setInteger()
                                                ->setUnsigned()
                                                ->setMaxIntegerDigits(10)),
                'is_active'                 => new Field
                                            (new BitValidator())
            ];
        }
    }
