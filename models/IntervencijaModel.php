<?php
    namespace App\Models;

    use App\Core\Model;
    use App\Core\Field;
    use App\Validators\DateTimeValidator;
    use App\Validators\NumberValidator;
    use App\Validators\StringValidator;
    use \PDO;

    class IntervencijaModel extends Model {
        protected function getFields() {
            return [
                'intervencija_id'               => new Field(
                                            (new NumberValidator())
                                                ->setInteger()
                                                ->setUnsigned()
                                                ->setMaxIntegerDigits(10), false),
                'created_at'                        => new Field
                                            (new DateTimeValidator(), false),
                'usluga_id'                         => new Field(
                                            (new NumberValidator())
                                                ->setInteger()
                                                ->setUnsigned()
                                                ->setMaxIntegerDigits(10)),
                'pacijent_id'                       => new Field(
                                            (new NumberValidator())
                                                ->setInteger()
                                                ->setUnsigned()
                                                ->setMaxIntegerDigits(10)),
                'zub'                               => new Field(
                                            (new StringValidator())
                                                ->setMinLength(2)
                                                ->setMaxLength(2)),
                'paket_id'                      => new Field(
                                            (new NumberValidator())
                                                ->setInteger()
                                                ->setUnsigned()
                                                ->setMaxIntegerDigits(10))
            ];
        }

        public function getAllIntervencijaByPacijentId($id): array {
            $pdo = $this->getDatabaseConnection()->getConnection();
            $sql = 'SELECT * FROM intervencija WHERE pacijent_id = ?;';
            $prep = $pdo->prepare($sql);
            $items = [];

            if ($prep) {
                $res = $prep->execute( [ $id ] );

                if ($res) {
                    $items = $prep->fetchAll(PDO::FETCH_OBJ);
                }
            }

            return $items;
        }
    }
