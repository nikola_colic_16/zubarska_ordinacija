var suma = 0;

function addPaket(id) {
    if(validatePaket()){
        let paketElement = document.getElementById('paket');
        let paketId = paketElement.options[paketElement.selectedIndex].value;
        let paket = paketElement.options[paketElement.selectedIndex].text;
        var formData = new FormData();
        formData.append('paket',paket);
        formData.append('paketId',paketId);
        formData.append('intervencija_name',paketId+'00');
        fetch('/zubarskaOrdinacija/api/intervencije/dodaj/paket/' + id , {
            method: 'post',
            credentials: "include",
            body: formData
        })
        .then(response => response.json())
        .then(json => {
            if(json['failed']){
                document.getElementById('message').classList.remove('d-none');
                document.getElementById('message').innerHTML = json['failed'];
            }
            else{
                document.getElementById('message').innerHTML = '';
                document.getElementById('message').classList.add('d-none');
            }
            document.getElementById('success').classList.remove('d-block');
            document.getElementById('success').classList.add('d-none');
            document.getElementById('success').innerHTML = '';
            getIntervencije(id);
        });
    }
}

function addIntervencije(id) {
    if(validateSemaZuba()){
        let zub = document.querySelector('input[name="zub"]:checked').value;
        let uslugaElement = document.getElementById('usluga');
        let uslugaId = uslugaElement.options[uslugaElement.selectedIndex].value;
        let usluga = uslugaElement.options[uslugaElement.selectedIndex].text;
        var formData = new FormData();
        formData.append('zub',zub);
        formData.append('usluga',usluga);
        formData.append('uslugaId',uslugaId);
        formData.append('intervencija_name',zub+uslugaId);
        fetch('/zubarskaOrdinacija/api/intervencije/dodaj/usluga/' + id , {
            method: 'post',
            credentials: "include",
            body: formData
        })
        .then(response => response.json())
        .then(json => {
            if(json['failed']){
                document.getElementById('message').classList.remove('d-none');
                document.getElementById('message').innerHTML = json['failed'];
            }
            else{
                document.getElementById('message').innerHTML = '';
                document.getElementById('message').classList.add('d-none');
            }
            document.getElementById('success').classList.remove('d-block');
            document.getElementById('success').classList.add('d-none');
            document.getElementById('success').innerHTML = '';
            getIntervencije(id);
        });
    }
}

function clearIntervencije(id, i) {
    //var id = window.location.href.replace('#', '').split('/').pop();
    fetch('/zubarskaOrdinacija/api/intervencije/obrisi/' + id+','+i, {
        credentials: "include"
    })
    .then(response => response.json())
    .then(json => {
        getIntervencije(id);
    });
    
}

function getIntervencije(id) {
    //var id = window.location.href.replace('#', '').split('/').pop();
    fetch('/zubarskaOrdinacija/api/intervencije/' + id, {
        credentials: "include",
    })
    .then(response => response.json())
    .then(json => {
        let intervencije = json;
        suma = intervencije['intervencijeApi' + id]['suma'];
        displayIntervencije(intervencije['intervencijeApi' + id], id);
    });
}

function displayIntervencije(intervencije, id) {
    if(intervencije.suma === 0 || intervencije.length === 0){
        document.getElementById('trenutno').style.display ='none' ;
        return;
    }
    var table = document.getElementById('trenutne-intervencije');
    var old_tbody = document.getElementById('tbody');
    var new_tBody = document.createElement('tbody');
    for (intervencija in intervencije) {
        if(intervencija === 'suma'){
            break;
        }
        let tr = document.createElement('tr');
        let tdZub = document.createElement('td');
        tdZub.innerText = intervencije[intervencija].zub;
        let tdNaziv = document.createElement('td');
        tdNaziv.innerText = intervencije[intervencija].naziv;
        let tdCena = document.createElement('td');
        tdCena.innerText = intervencije[intervencija].cena;
        let tdButton = document.createElement('td');
        new_tBody.appendChild(tr);
        
        let clearButton = document.createElement('button');
        clearButton.innerHTML = '<i class="fa fa-window-close" aria-hidden="true"></i>';
        clearButton.setAttribute('id','clear-button'+intervencija);
        clearButton.setAttribute('class','btn btn-danger');
        clearButton.setAttribute('type','button');
        clearButton.setAttribute('onclick', 'clearIntervencije('+id+','+intervencija+')');
        tdButton.append(clearButton);
        
        tr.appendChild(tdZub);
        tr.appendChild(tdNaziv);
        tr.appendChild(tdCena);
        tr.appendChild(tdButton);
    }
    table.replaceChild(new_tBody, old_tbody);
    new_tBody.setAttribute('id','tbody')
    document.getElementById('suma').innerHTML = suma;
    document.getElementById('trenutno').style.display='block';
}

function zakljuciIntervenciju(id){

}
