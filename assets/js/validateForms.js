function validateUslugeForms() {
    let status = true;
    document.querySelector('#error-message').innerHTML = '';
    document.querySelector('#error-message').classList.add('d-none');

    let naziv = document.getElementById('naziv').value;
    if (!naziv.match(/.*[^\s]{3,}.*/)) {
        document.querySelector('#error-message').innerHTML += 'Naziv mora sadržati najmanje 3 vidljiva uzastopna karaktera.<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    let opis = document.getElementById('opis').value;
    if (!opis.match(/.*[^\s]{5,}.*/)) {
        document.querySelector('#error-message').innerHTML += 'Opis mora sadržati najmanje 5 vidljivih uzastopnih karaktera.<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    let kataloski_broj = document.getElementById('kataloski_broj').value;
    if (!kataloski_broj.match(/^[0-9]{4,10}$/)) {
        document.querySelector('#error-message').innerHTML += 'Kataloški broj se mora sastojati od brojeva dužine 4 do 10.<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    
    if(document.getElementById('cena_dete') !== null){
        let cena = document.getElementById('cena_dete').value;
        if(!validateCena(cena)){
            status = false;
        }
        cena = document.getElementById('cena_penzioner').value;
        if(!validateCena(cena)){
            status = false;
        }
        cena = document.getElementById('cena_ostali').value;
        if(!validateCena(cena)){
            status = false;
        }
    }
    else{
        let cena = document.getElementById('cena').value;
        if(!validateCena(cena)){
            status = false;
        }
    }
        
    
    return status;
}

function validateCena(cena){
    if (!cena.match(/^[0-9]{2,8}(\.[0-9]{1,2})?$/)) {
        document.querySelector('#error-message').innerHTML += 'Cena mora imati minimum 2 celobrojne cifre, a maksimum 10 cifara uključujući decimale i ne može biti negativna vrednost.<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        return false;
    }
    return true;
}

function validatePacijentiForms() {
    let status = true;
    document.querySelector('#error-message').innerHTML = '';
    document.querySelector('#error-message').classList.add('d-none');

    let ime = document.getElementById('ime').value;
    if (!ime.match(/.*[^\s]{3,}.*/)) {
        document.querySelector('#error-message').innerHTML += 'Ime mora sadržati najmanje 3 vidljiva uzastopna karaktera.<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    let prezime = document.getElementById('prezime').value;
    if (!prezime.match(/.*[^\s]{3,}.*/)) {
        document.querySelector('#error-message').innerHTML += 'Prezime mora sadržati najmanje 3 vidljiva uzastopna karaktera.<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    let adresa = document.getElementById('adresa').value;
    if (!adresa.match(/^.{2,150}$/)) {
        document.querySelector('#error-message').innerHTML += 'Adresa mora sadržati najmanje 2, a najviše 150 karaktera.<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    let born_at = document.getElementById('born_at').value;
    
    if (!born_at.match(/^(19[0-9]{2}|20[0,1][0-9])\-(0[1-9]|1[0-2])\-(0[1-9]|[1-2][0-9]|30|31)$/)) {
        document.querySelector('#error-message').innerHTML += 'Unesite realan datum rođenja.<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }

    let telefon = document.getElementById('telefon').value;
    if (!telefon.match(/^\+[0-9]{6,24}$/)) {
        document.querySelector('#error-message').innerHTML += 'Telefonski broj mora da pocinje sa + iza kojeg sledi od 6 do 24 cifara.<br>';
        document.querySelector('#error-message').classList.remove('d-none');
        status = false;
    }
    
    return status;
}

function validateSemaZuba(){
    let status = true;

    document.querySelector('#error-message-usluga').innerHTML = '';
    document.querySelector('#error-message-usluga').classList.add('d-none');

    if ($('input[name=zub]:checked').length === 0) {
        document.querySelector('#error-message-usluga').innerHTML += 'Izaberite nad kojim zubom je vršena intervencija.<br>';
        document.querySelector('#error-message-usluga').classList.remove('d-none');
        status = false;
    }
    let usluga = document.getElementById('usluga').value;
    if (usluga == '') {
        document.querySelector('#error-message-usluga').innerHTML += 'Morate izabrati uslugu.<br>';
        document.querySelector('#error-message-usluga').classList.remove('d-none');
        status = false;
    }

    return status;
}

function validatePaket(){
    let status = true;

    document.querySelector('#error-message-paket').innerHTML = '';
    document.querySelector('#error-message-paket').classList.add('d-none');

    let paket = document.getElementById('paket').value;
    if (paket == '') {
        document.querySelector('#error-message-paket').innerHTML += 'Morate izabrati paket.<br>';
        document.querySelector('#error-message-paket').classList.remove('d-none');
        status = false;
    }

    return status;

}