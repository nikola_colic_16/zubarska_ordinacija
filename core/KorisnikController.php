<?php
    namespace App\Core;

    class KorisnikController extends Controller {
        public function __pre() {
            if ($this->getSession()->get('userId') === null) {
                ob_clean();
                header('Location: ' . BASE . 'korisnik/prijava', true, 307);
                exit;
            }
        }
    }
