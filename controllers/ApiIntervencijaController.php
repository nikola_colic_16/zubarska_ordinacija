<?php
namespace App\Controllers;

use App\Core\ApiController;
use App\Models\PacijentModel;
use App\Models\UslugaKategorijaPacijentaModel;
use App\Models\PaketKategorijaPacijentaModel;
use App\Validators\ZubValidator;
use App\Validators\NumberValidator;

class ApiIntervencijaController extends ApiController{
    public function getIntervencije($id){
        $intervencijeApi = $this->getSession()->get('intervencijeApi' . $id, []);
        $this->set('intervencijeApi' . $id, $intervencijeApi);
        $this->set('error', 0);
    }

    public function addIntervencije($id){
        $intervencijeApi = $this->getSession()->get('intervencijeApi' . $id , []);
        $zub = filter_input(INPUT_POST, 'zub', FILTER_SANITIZE_NUMBER_INT);
        $uslugaId = filter_input(INPUT_POST, 'uslugaId', FILTER_SANITIZE_NUMBER_INT);
        $usluga = filter_input(INPUT_POST, 'usluga', FILTER_SANITIZE_STRING);
        $intervencija_name = filter_input(INPUT_POST, 'intervencija_name', FILTER_SANITIZE_NUMBER_INT);
        $intervencijeApi = json_decode(json_encode($intervencijeApi), true);

        $validator = new ZubValidator();
        if (! $validator->isValid($zub)){
            $this->set('failed', 'Neispravna vrednost za zub.');
            return;
        }
        $validator = (new NumberValidator())->setInteger()->setUnsigned();
        if (! $validator->isValid($uslugaId)){
            $this->set('failed', 'Neispravna vrednost za uslugu.');
            return;
        }
        $validator = (new NumberValidator())->setInteger()->setUnsigned()->setMinIntegerDigits(3);
        if (! $validator->isValid($intervencija_name)){
            $this->set('failed', 'Neispravna vrednost za uslugu ili zub.');
            return;
        }
        foreach($intervencijeApi as $key => $value){
            if(substr($key,0,2) == $zub){
                $this->set('failed', 'Već ste dodali intervenciju za ovaj zub.');
                return;
            }
        }

        $pm = new PacijentModel($this->getDatabaseConnection());
        $ukpm = new UslugaKategorijaPacijentaModel($this->getDatabaseConnection());
        $kategorija_pacijenta_id = $pm->getById($id)->kategorija_pacijenta_id;
        $cena = $ukpm->getByUslugaKategorija($uslugaId, $kategorija_pacijenta_id)->cena;

        $intervencija = [
            'zub'       => $zub,
            'id'    => $uslugaId, 
            'naziv'    => $usluga, 
            'cena'    => $cena 
        ];
        $intervencijeApi[$intervencija_name] = $intervencija;


        if(!isset($intervencijeApi['suma'])){
            $intervencijeApi['suma'] = $cena;
        }
        else{
            $intervencijeApi['suma'] +=  $cena;
        }
        $this->getSession()->put('intervencijeApi' . $id , $intervencijeApi);
        $this->set('error', 0);
    }
    public function addPaket($id){
        $intervencijeApi = $this->getSession()->get('intervencijeApi' . $id , []);
        $paketId = filter_input(INPUT_POST, 'paketId', FILTER_SANITIZE_NUMBER_INT);
        $paket = filter_input(INPUT_POST, 'paket', FILTER_SANITIZE_STRING);
        $intervencija_name = filter_input(INPUT_POST, 'intervencija_name', FILTER_SANITIZE_NUMBER_INT);
        $intervencijeApi = json_decode(json_encode($intervencijeApi), true);

        foreach($intervencijeApi as $key => $value){
            if(substr($key,1) == 00){
                if(substr($key,0,1) == $paketId){
                    $this->set('failed', 'Već ste primenili ovaj paket.');
                    return;
                }
            }
        }

        $pm = new PacijentModel($this->getDatabaseConnection());
        $pkpm = new PaketKategorijaPacijentaModel($this->getDatabaseConnection());
        $kategorija_pacijenta_id = $pm->getById($id)->kategorija_pacijenta_id;
        $cena = $pkpm->getByPaketKategorija($paketId, $kategorija_pacijenta_id)->cena;

        $intervencija = [
            'zub'       => '00',
            'id'    => $paketId, 
            'naziv'    => $paket, 
            'cena'    => $cena 
        ];
        $intervencijeApi[$intervencija_name] = $intervencija;


        if(!isset($intervencijeApi['suma'])){
            $intervencijeApi['suma'] = $cena;
        }
        else{
            $intervencijeApi['suma'] +=  $cena;
        }
        $this->getSession()->put('intervencijeApi' . $id , $intervencijeApi);
        $this->set('error', 0);
    }

    public function clear($id, $i) {
        $intervencijeApi = $this->getSession()->get('intervencijeApi' . $id, []);
        $intervencijeApi = json_decode(json_encode($intervencijeApi), true);
        $intervencijeApi['suma'] -= $intervencijeApi[$i]['cena'];
        unset($intervencijeApi[$i]);
        $this->getSession()->put('intervencijeApi' . $id , $intervencijeApi);
        $this->set('error', 0);
    }

    public function sesija(){
        print_r(  $this->getSession()->keys('intervencijeApi4', []) );
        exit;
    }
}