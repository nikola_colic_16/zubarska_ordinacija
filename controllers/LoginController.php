<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Models\KorisnikModel;
use App\Models\PrijavaModel;

class LoginController extends Controller {
    public function loginGet() {
        if ($this->getSession()->get('userId') !== null) {
            ob_clean();
            header('Location: ' . BASE , true, 307);
            exit;
        }
    }

    public function logoutGet(){
        $this->getSession()->remove('userId');
        $this->getSession()->save();
        header('Location: ' . BASE );
        exit;
    }

    public function loginPost() {
        $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

        $km = new KorisnikModel($this->getDatabaseConnection());
        $pm = new PrijavaModel($this->getDatabaseConnection());

        $korisnik = $km->getByFieldName('korisnicko_ime', $username);
        if (!$korisnik) {
            sleep(1);
            $this->set('message', 'Loši podaci!');
            return;
        }
        if (!password_verify($password, $korisnik->lozinka_hash)) {
            $prijavaId = $pm->add([
                'korisnik_id'   => $korisnik->korisnik_id,
                'ip_adresa'     => filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_SANITIZE_STRING),
                'is_valid'      => '0'
                ]);
            sleep(1);
            $this->set('message', 'Loši podaci!');
            return;
        }
        $pm->add([
            'korisnik_id'   => $korisnik->korisnik_id,
            'ip_adresa'     => filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_SANITIZE_STRING),
            'is_valid'      => '1'
            ]);

        $this->getSession()->put('userId', $korisnik->korisnik_id);

        \ob_clean();
        header('Location: ' . BASE );
        exit;
    }
}
