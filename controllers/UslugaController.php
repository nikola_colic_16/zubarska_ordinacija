<?php
namespace App\Controllers;

use App\Models\UslugaModel;
use App\Core\KorisnikController;
use App\Models\TipUslugeModel;
use App\Models\UslugaKategorijaPacijentaModel;
use App\Models\KategorijaPacijentaModel;
use App\Validators\WhitespaceStringValidator;
use App\Validators\NumberValidator;

class UslugaController extends KorisnikController {
    public function usluge() {
        $um = new UslugaModel($this->getDatabaseConnection());
        $tum = new TipUslugeModel($this->getDatabaseConnection());
        $usluge = $um->getAllOrderedByFieldName('is_visible', 'DESC');
        foreach($usluge as $usluga){
            $tip = $tum->getById($usluga->tip_usluge_id);
            $usluga->naziv_tipa = $tip->naziv_tipa;
            $cenePoKategoriji = $um->getCenePoKategoriji($usluga->usluga_id);
            $usluga->cene = $cenePoKategoriji;
        }
        
        $this->set('usluge', $usluge);
    }

    public function getEdit($id) {
        $um = new UslugaModel($this->getDatabaseConnection());
        $tum = new TipUslugeModel($this->getDatabaseConnection());

        $usluga = $um->getById($id);
        if (!$usluga) {
            \ob_clean();
            header('Location: ' . BASE . 'usluge');
            exit;
        }
        $tipoviUsluga = $tum->getAll();
        $cenePoKategoriji = $um->getCenePoKategoriji($id);

        $this->set('usluga', $usluga);
        $this->set('cenePoKategoriji', $cenePoKategoriji);
        $this->set('tipoviUsluga', $tipoviUsluga);
    }

    public function postEdit($id) {
        $naziv = filter_input(INPUT_POST, 'naziv', FILTER_SANITIZE_STRING);
        $opis = filter_input(INPUT_POST, 'opis', FILTER_SANITIZE_STRING);
        $kataloski_broj = filter_input(INPUT_POST, 'kataloski_broj', FILTER_SANITIZE_STRING);
        $is_visible = filter_input(INPUT_POST, 'is_visible', FILTER_SANITIZE_NUMBER_INT);
        $tip = filter_input(INPUT_POST, 'tip_usluge', FILTER_SANITIZE_NUMBER_INT);
        $kp_id = filter_input(INPUT_POST, 'kategorija_pacijenta', FILTER_SANITIZE_NUMBER_INT);
        $cena = filter_input(INPUT_POST, 'cena', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

        
        if(!$this->validateInputs($naziv, $opis, $kataloski_broj, $id)){
            return;
        }
        if(!$this->validateCena($cena, $id)){
            return;
        }

        if (! preg_match('|^[0-1]$|',$is_visible)){
            $this->set('message', 'Uneta neočekivana vrednost za skrivanje usluge.');
            $this->set('usluga_id', $id);
            return;
        }
        
        $um = new UslugaModel($this->getDatabaseConnection());
        $ukpm = new UslugaKategorijaPacijentaModel($this->getDatabaseConnection());

        $uslugaKategorijaPacijenta = $ukpm->getByUslugaKategorija($id, $kp_id);
        $ukp_id = $uslugaKategorijaPacijenta->usluga_kategorija_pacijenta_id;

        $resUsluge = $um->editById($id, [
            'naziv' => $naziv,
            'opis' => $opis,
            'kataloski_broj' => $kataloski_broj,
            'tip_usluge_id' => $tip,
            'is_visible' => $is_visible
        ]);

        if (!$resUsluge) {
            $this->set('message', 'Došlo je do greške prilikom izmene podataka ove usluge.');
            return;
        }

        $resCene = $ukpm->editById($ukp_id, [
            'usluga_id' => $id,
            'kategorija_pacijenta_id' => $kp_id,
            'cena' => $cena
        ]);
        
        if (!$resCene) {
            $this->set('message', 'Došlo je do greške prilikom izmene cene ove usluge.');
            return;
        }

        \ob_clean();
        header('Location: ' . BASE . 'usluge');
        exit;
    }

    public function getAdd(){
        $tum = new TipUslugeModel($this->getDatabaseConnection());
        $tip_usluge = $tum->getAll();
        $this->set('tip_usluge', $tip_usluge);
    }

    public function postAdd(){
        $naziv = filter_input(INPUT_POST, 'naziv', FILTER_SANITIZE_STRING);
        $opis = filter_input(INPUT_POST, 'opis', FILTER_SANITIZE_STRING);
        $kataloski_broj = filter_input(INPUT_POST, 'kataloski_broj', FILTER_SANITIZE_STRING);
        $tip = filter_input(INPUT_POST, 'tip_usluge', FILTER_SANITIZE_NUMBER_INT);
        $cena_dete = filter_input(INPUT_POST, 'cena_dete', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $cena_penzioner = filter_input(INPUT_POST, 'cena_penzioner', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $cena_ostali = filter_input(INPUT_POST, 'cena_ostali', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

        if(!$this->validateInputs($naziv, $opis, $kataloski_broj)){
            return;
        }
        if(!$this->validateCena($cena_dete)){
            return;
        }
        if(!$this->validateCena($cena_penzioner)){
            return;
        }
        if(!$this->validateCena($cena_ostali)){
            return;
        }

        $um = new UslugaModel($this->getDatabaseConnection());
        $ukpm = new UslugaKategorijaPacijentaModel($this->getDatabaseConnection());
        $kpm = new KategorijaPacijentaModel($this->getDatabaseConnection());

        
        $kategorije = [
            $kpm->getByFieldName('naziv', 'Dete')->kategorija_pacijenta_id => $cena_dete,
            $kpm->getByFieldName('naziv', 'Penzioner')->kategorija_pacijenta_id => $cena_penzioner,
            $kpm->getByFieldName('naziv', 'Ostali')->kategorija_pacijenta_id => $cena_ostali 
        ];
        $usluga = $um->add([
            'naziv' => $naziv,
            'opis' => $opis,
            'kataloski_broj' => $kataloski_broj,
            'tip_usluge_id' => $tip,
            'is_visible' => 1
        ]);
        
        if(!$usluga){
            $this->set('message', 'Došlo je do greške prilikom dodavanja ove usluge.');
            return;
        }

        foreach($kategorije as $kategorija_id => $cena){
            $cenaUsluge = $ukpm->add([
                'usluga_id' => $usluga,
                'kategorija_pacijenta_id' => $kategorija_id,
                'cena' => $cena,
            ]);
            if(!$cenaUsluge){
                $this->set('message', 'Došlo je do greške prilikom dodavanja cene ove usluge.');
                return;
            }
        }

        \ob_clean();
        header('Location: ' . BASE . 'usluge');
        exit;
    }

    private function validateInputs($naziv, $opis, $kataloski_broj, $id = false){
        $validator = (new WhitespaceStringValidator())->setMinLength(3)->setMaxLength(65);
        if (! $validator->matchPattern($naziv, 3)){
            $this->set('message', 'Naziv mora sadržati najmanje 3 vidljiva uzastopna karaktera.');
            if($id){
                $this->set('usluga_id', $id);
            }
            return false;
        }

        $validator = (new WhitespaceStringValidator())->setMinLength(5);
        if (! $validator->matchPattern($opis, 5)){
            $this->set('message', 'Opis mora sadržati najmanje 5 vidljivih uzastopna karaktera.');
            if($id){
                $this->set('usluga_id', $id);
            }
            return false;
        }

        $validator = (new NumberValidator())->setInteger()->setMinIntegerDigits(4)->setUnsigned();
        if (! $validator->isValid($kataloski_broj)){
            $this->set('message', 'Kataloški broj se mora sastojati od brojeva dužine 4 do 10.');
            if($id){
                $this->set('usluga_id', $id);
            }
            return false;
        }
        return true;
    }

    private function validateCena($c, $id = false){
        $validator = (new NumberValidator())->setMinIntegerDigits(2)->setMaxIntegerDigits(8)->setUnsigned();
        if (! $validator->isValid($c)){
            $this->set('message', 'Cena mora imati minimum 2 celobrojne cifre, a maksimum 10 cifara uključujući decimale i ne može biti negativna vrednost.');
            if($id){
                $this->set('usluga_id', $id);
            }
            return false;
        }
        return true;
    }


}
