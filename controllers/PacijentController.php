<?php
namespace App\Controllers;

use App\Models\PacijentModel;
use App\Core\KorisnikController;
use App\Models\TipUslugeModel;
use App\Models\KategorijaPacijentaModel;
use App\Validators\WhitespaceStringValidator;
use App\Validators\DateOfBirthValidator;

class PacijentController extends KorisnikController {
    public function pacijenti() {
        $pm = new PacijentModel($this->getDatabaseConnection());
        $kpm = new KategorijaPacijentaModel($this->getDatabaseConnection());
        $pacijenti = $pm->getAllOrderedByFieldName('is_active', 'DESC');
        foreach($pacijenti as $pacijent){
            $kategorija =$kpm->getById($pacijent->kategorija_pacijenta_id);
            $pacijent->kategorija = $kategorija->naziv;
        }
        $this->set('pacijenti', $pacijenti);
    }

    public function getEdit($id) {
        $pm = new PacijentModel($this->getDatabaseConnection());
        $kpm = new KategorijaPacijentaModel($this->getDatabaseConnection());
        $pacijent = $pm->getById($id);
        $kategorije = $kpm->getAll();
        $this->set('pacijent', $pacijent);
        $this->set('kategorije', $kategorije);
    }

    public function postEdit($id) {
        $ime = filter_input(INPUT_POST, 'ime', FILTER_SANITIZE_STRING);
        $prezime = filter_input(INPUT_POST, 'prezime', FILTER_SANITIZE_STRING);
        $adresa = filter_input(INPUT_POST, 'adresa', FILTER_SANITIZE_STRING);
        $born_at = filter_input(INPUT_POST, 'born_at', FILTER_SANITIZE_STRING);
        $telefon = filter_input(INPUT_POST, 'telefon', FILTER_SANITIZE_STRING);
        $kp_id = filter_input(INPUT_POST, 'kategorija_pacijenta', FILTER_SANITIZE_NUMBER_INT);
        $is_active = filter_input(INPUT_POST, 'is_active', FILTER_SANITIZE_NUMBER_INT);

        if(!$this->validateInputs($ime, $prezime, $adresa, $born_at,$telefon,$kp_id, $id)){
            return;
        }
        if (! preg_match('|^[0-1]$|',$is_active)){
            $this->set('message', 'Uneta neočekivana vrednost za aktivnost pacijenta.');
            $this->set('usluga_id', $id);
            return;
        }

        $pacijent = $pm = new PacijentModel($this->getDatabaseConnection());

        $pacijent->editById($id, [
            'ime'                       => $ime,
            'prezime'                   => $prezime,
            'adresa'                    => $adresa,
            'born_at'                   => $born_at,
            'telefon'                   => $telefon,
            'kategorija_pacijenta_id'   => $kp_id,
            'is_active'                 => $is_active,
        ]);

        if(!$pacijent){
            $this->set('message', 'Došlo je do greške prilikom izmene pacijenta.');
            return;
        }

        \ob_clean();
        header('Location: ' . BASE . 'pacijenti');
        exit;
    }

    public function getAdd() {
        $kpm = new KategorijaPacijentaModel($this->getDatabaseConnection());
        $kategorije = $kpm->getAll();
        $this->set('kategorije', $kategorije);
    }

    public function postAdd() {
        $ime = filter_input(INPUT_POST, 'ime', FILTER_SANITIZE_STRING);
        $prezime = filter_input(INPUT_POST, 'prezime', FILTER_SANITIZE_STRING);
        $adresa = filter_input(INPUT_POST, 'adresa', FILTER_SANITIZE_STRING);
        $born_at = filter_input(INPUT_POST, 'born_at', FILTER_SANITIZE_STRING);
        $telefon = filter_input(INPUT_POST, 'telefon', FILTER_SANITIZE_STRING);
        $kp_id = filter_input(INPUT_POST, 'kategorija_pacijenta', FILTER_SANITIZE_NUMBER_INT);

        if(!$this->validateInputs($ime, $prezime, $adresa, $born_at,$telefon,$kp_id)){
            return;
        }

        $pm = new PacijentModel($this->getDatabaseConnection());

        $pacijent = $pm->add([
            'ime' => $ime,
            'prezime' => $prezime,
            'adresa' => $adresa,
            'born_at' => $born_at,
            'telefon' => $telefon,
            'kategorija_pacijenta_id' => $kp_id,
            'is_active' => 1
        ]);

        if(!$pacijent){
            $this->set('message', 'Došlo je do greške prilikom dodavanja novog pacijenta.');
            return;
        }

        \ob_clean();
        header('Location: ' . BASE . 'pacijenti');
        exit;
    }

    private function validateInputs($ime, $prezime, $adresa, $born_at,$telefon,$kp_id, $id = false){
        $validator = (new WhitespaceStringValidator())->setMinLength(3)->setMaxLength(45);
        if (! $validator->matchPattern($ime, 3)){
            $this->set('message', 'Ime mora sadržati najmanje 3 vidljiva uzastopna karaktera.');
            if($id){
                $this->set('usluga_id', $id);
            }
            return false;
        }
        $validator = (new WhitespaceStringValidator())->setMinLength(3)->setMaxLength(45);
        if (! $validator->matchPattern($prezime, 3)){
            $this->set('message', 'Prezime mora sadržati najmanje 3 vidljiva uzastopna karaktera.');
            if($id){
                $this->set('usluga_id', $id);
            }
            return false;
        }

        $validator = (new WhitespaceStringValidator())->setMinLength(2)->setMaxLength(150);
        if (! $validator->matchPattern($adresa, 5)){
            $this->set('message', 'Adresa mora sadržati najmanje 2, a najviše 150 karaktera.');
            if($id){
                $this->set('usluga_id', $id);
            }
            return false;
        }

        $validator = (new DateOfBirthValidator())->disallowTime();
        if (! $validator->isReal($born_at)){
            $this->set('message', 'Unesite realan datum rođenja.');
            if($id){
                $this->set('usluga_id', $id);
            }
            return false;
        }
        if (! preg_match('|^\+[0-9]{6,24}$|',$telefon)){
            $this->set('message', 'Telefonski broj mora da pocinje sa + iza kojeg sledi od 6 do 24 cifara.');
            $this->set('usluga_id', $id);
            return;
        }
        if (! preg_match('|^[1-3]$|',$kp_id)){
            $this->set('message', 'Uneta neočekivana vrednost za kategoriju pacijenta.');
            $this->set('usluga_id', $id);
            return;
        }
        return true;
    }
}


