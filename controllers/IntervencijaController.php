<?php
namespace App\Controllers;

use App\Models\PacijentModel;
use App\Core\KorisnikController;
use App\Models\IntervencijaModel;
use App\Models\UslugaModel;
use App\Models\PaketKategorijaPacijentaModel;
use App\Models\PaketModel;
use App\Validators\ZubValidator;

class IntervencijaController extends KorisnikController {

    public function karton($id) {
        $im = new IntervencijaModel($this->getDatabaseConnection());
        $um = new UslugaModel($this->getDatabaseConnection());
        $pakm = new PaketModel($this->getDatabaseConnection());
        $pacm = new PacijentModel($this->getDatabaseConnection());
        $pkpm = new PaketKategorijaPacijentaModel($this->getDatabaseConnection());
        $intervencije = $im->getAllIntervencijaByPacijentId($id);
        foreach($intervencije as $intervencija){
            if($intervencija->usluga_id){
                $usluga = $um->getById($intervencija->usluga_id);
                $intervencija->usluga_id = $usluga->naziv;
            }
            elseif($intervencija->paket_id){
                $paket = $pakm->getById($intervencija->paket_id);
                $intervencija->paket_id = $paket->naziv;
            }
        }
        $usluge = $um->getAllByFieldName('is_visible', 1);
        $pacijent = $pacm->getById($id);
        $paketi_kategorije = $pkpm->getAllByFieldName('kategorija_pacijenta_id', $pacijent->kategorija_pacijenta_id);
        if($paketi_kategorije){
            $paketi = $pakm->getAll();
            foreach($paketi_kategorije as $paket_kategorija){
                foreach($paketi as $paket){
                    if($paket_kategorija->paket_id == $paket->paket_id)
                        $paket_kategorija->naziv = $paket->naziv;
                }
            }
        }
        $this->set('pacijent', $pacijent);
        $this->set('intervencije', $intervencije);
        $this->set('usluge', $usluge);
        $this->set('paketi_kategorije', $paketi_kategorije);

        if($this->getSession()->exists('success')){
            $this->set('success', 'Uspešno ste dodali intervencije.');
        }
        $this->getSession()->remove('success');
        if($this->getSession()->exists('failed')){
            $this->set('failed', 'Neuspešno dodavanje intervencije, pokušajte ponovo.');
        }
        $this->getSession()->remove('failed');
    }

    public function zakljuciIntervenciju($pacijent_id) {
        $im = new IntervencijaModel($this->getDatabaseConnection());
        $intervencijeApi = $this->getSession()->get('intervencijeApi' . $pacijent_id, []);
        $intervencijeApi = json_decode(json_encode($intervencijeApi), true);
        unset($intervencijeApi['suma']);
        foreach($intervencijeApi as $intervencija){
            $validator = new ZubValidator();
            if (! $validator->isValid($intervencija['zub'])){
                $this->set('message', 'Neispravna vrednost za zub.');
                $this->set('pacijent_id', $pacijent_id);
                $this->getSession()->put('intervencijeApi' . $pacijent_id, []);
        
                $this->getSession()->put('failed', 'Neuspešno dodavanje intervencije, pokušajte ponovo.');
                return;
            }
            if($intervencija['zub'] == '00'){   //  PAKET
                $params = [
                    'pacijent_id'   => $pacijent_id,
                    'zub'           => $intervencija['zub'],
                    'paket_id'      => $intervencija['id']
                ];
                $paket = $im->add($params);

                if(!$paket){
                    $this->set('message', 'Došlo je do greške prilikom primene paketa.');
                    $this->set('pacijent_id', $pacijent_id);
                    return;
                }
            }
            else{                               //  USLUGA
                $params = [
                    'usluga_id'     => $intervencija['id'],
                    'pacijent_id'   => $pacijent_id,
                    'zub'           => $intervencija['zub'],
                ];
                $usluga = $im->add($params);

                if(!$usluga){
                    $this->set('message', 'Došlo je do greške prilikom dodavanja usluge.');
                    $this->set('pacijent_id', $pacijent_id);
                    return;
                }
            }
        }
        $this->getSession()->put('intervencijeApi' . $pacijent_id, []);
        
        $this->getSession()->put('success', 'Uspešno ste dodali intervencije.');
        \ob_clean();
        header('Location: ' . BASE . 'karton/'. $pacijent_id);
        exit;
    }
}
