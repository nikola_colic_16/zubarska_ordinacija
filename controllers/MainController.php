<?php
namespace App\Controllers;

use App\Models\UslugaModel;
use App\Core\KorisnikController;
use App\Models\KorisnikModel;
use App\Validators\StringValidator;

class MainController extends KorisnikController {
    public function home() {
        ob_clean();
        header('Location: ' . BASE . 'usluge', true, 307);
        exit;
    }
}
