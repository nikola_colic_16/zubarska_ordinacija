<?php
    namespace App\Validators;

    use App\Core\Validator;

    class DateOfBirthValidator extends DateTimeValidator {
        public function isReal($value){
            if(!$this->isValid($value)){
                return false;
            }
            $date = explode('-', $value);
            if($date[0] > date('Y') || $date[0] < 1900){
                return false;
            }
            if($date[0] == date('Y')){
                if($date[1] == date('m')){
                    if($date[2] >= date('d')){
                        return false;
                    }
                }
            }
            return true;
        }
    }
