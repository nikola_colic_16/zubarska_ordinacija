<?php
    namespace App\Validators;

    use App\Core\Validator;

    class WhitespaceStringValidator extends StringValidator {
        public function matchPattern(string $value, string $whitespace) {
            if(!\preg_match('|.*[^\s]{' . $whitespace . ',}.*|', $value)){
                return false;
            }
            return $this->isValid($value);

        }
    }