<?php
    use App\Core\Route;

    return [
        Route::get('#^korisnik/prijava/?$#',                                'Login',                'loginGet'),
        Route::post('#^korisnik/prijava/?$#',                               'Login',                'loginPost'),
        Route::get('#^korisnik/odjava/?$#',                                 'Login',                'logoutGet'),
    
        Route::get('#^usluge/?$#',                                          'Usluga',               'usluge'),
        Route::get('#^usluge/izmeni/([0-9]+)/?$#',                          'Usluga',               'getEdit'),
        Route::post('#^usluge/izmeni/([0-9]+)/?$#',                         'Usluga',               'postEdit'),
        Route::get('#^usluge/dodaj/?$#',                                    'Usluga',               'getAdd'),
        Route::post('#^usluge/dodaj/?$#',                                   'Usluga',               'postAdd'),

        Route::get('#^pacijenti/?$#',                                       'Pacijent',             'pacijenti'),
        Route::get('#^pacijenti/izmeni/([0-9]+)/?$#',                       'Pacijent',             'getEdit'),
        Route::post('#^pacijenti/izmeni/([0-9]+)/?$#',                      'Pacijent',             'postEdit'),
        Route::get('#^pacijenti/dodaj/?$#',                                 'Pacijent',             'getAdd'),
        Route::post('#^pacijenti/dodaj/?$#',                                'Pacijent',             'postAdd'),

        Route::get('#^api/intervencije/([0-9]+)/?$#',                       'ApiIntervencija',      'getIntervencije'),
        Route::post('#^api/intervencije/dodaj/usluga/([0-9]+)/?$#',         'ApiIntervencija',      'addIntervencije'),
        Route::post('#^api/intervencije/dodaj/paket/([0-9]+)/?$#',          'ApiIntervencija',      'addPaket'),
        Route::get('#^api/intervencije/obrisi/([0-9]+),([0-9]+)/?$#',       'ApiIntervencija',      'clear'),

        Route::get('#^karton/([0-9]+)/?$#',                                 'Intervencija',         'karton'),
        Route::post('#^karton/([0-9]+)/?$#',                                'Intervencija',         'zakljuciIntervenciju'),
        # Fallback
         Route::get('#^.*$#',                                               'Main',                 'home')
    ];
